[![Documentation Status](https://readthedocs.org/projects/phasik/badge/)](http://phasik.readthedocs.io/)
[![codecov](https://codecov.io/gl/habermann_lab/phasik/branch/master/graph/badge.svg?token=8FXCNM1GSB)](https://codecov.io/gl/habermann_lab/phasik)
[![PyPI version](https://badge.fury.io/py/phasik.svg)](https://badge.fury.io/py/phasik)
[![PyPI license](https://img.shields.io/pypi/l/phasik.svg)](https://pypi.python.org/pypi/phasik/)
[![Downloads](https://static.pepy.tech/personalized-badge/phasik?period=total&units=international_system&left_color=grey&right_color=green&left_text=Downloads)](https://pepy.tech/project/phasik)

# Phasik 

## What is Phasik?
The Phasik package was created to infer temporal phases in temporal networks.  It contains various utility classes and functions that can be divided into two main parts:

1. Build, analyse, and visualise temporal networks from time series data.
2. Infer temporal phases by clustering the snapshots of the temporal network.  

Code on Gitlab: [https://gitlab.com/habermann_lab/phasik](https://gitlab.com/habermann_lab/phasik)  
Docs: <https://phasik.readthedocs.io/en/latest/>  
Tutorials: <https://phasik.readthedocs.io/en/latest/tutorial/index.html>  
Notebooks: <https://gitlab.com/habermann_lab/phasik/-/tree/master/notebooks>  

Phasik was initially created for the analysis presented in the paper "[Inferring cell cycle phases from a partially temporal network of protein interactions](https://doi.org/10.1101/2021.03.26.437187)" by Lucas, M., Morris, A., Townsend-Teague, A., Tichit, L., Habermann, B. H., & Barrat, A. (2021), bioRxiv.

In addition to the package, this repository contains the notebooks necessary to reproduce the analysis of the paper.  

DOI: 10.5281/zenodo.7378779

## Install Phasik 

Install the latest version of `phasik` with `pip`:

```
$ pip install phasik
```

Alternatively, you can clone the repository manually or with `git`, go to the repository and then install locally with `pip`:
```    
$ pip install .
```   
## Contributors 

Maxime Lucas, Alex Townsend-Teague, Arthur Morris

## Contact

ml.maximelucas[at]gmail[dot]com
