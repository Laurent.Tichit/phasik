from setuptools import find_packages, setup

VERSION_FILE = "phasik/_version.py"
with open(VERSION_FILE, "rt") as f:
    version_txt = f.read().strip()
    VERSION = version_txt.split('"')[1]


def parse_requirements_file(filename):
    with open(filename) as fid:
        requires = [
            line.strip() for line in fid.readlines() if not line.startswith("#")
        ]
    return requires


description = """Tools to build temporal networks and infer temporal phases from them"""


extras_require = {
    dep: parse_requirements_file("requirements/" + dep + ".txt")
    for dep in [
        "developer",
        "documentation",
        "release",
        "test",
        "tutorial",
    ]
}

extras_require["all"] = list({item for dep in extras_require.values() for item in dep})

install_requires = parse_requirements_file("requirements/default.txt")


setup(
    name="phasik",
    version=VERSION,
    author="Maxime Lucas",
    author_email="ml.maximelucas@gmail.com",
    description=description,
    long_description="Tools to build temporal networks and infer temporal phases from them. Build temporal networks from time series data. Cluster snapshots to infer the multiscale temporal organisation of the network.",
    url="https://gitlab.com/habermann_lab/phasik",
    packages=find_packages(),
    install_requires=install_requires,
    extras_require=extras_require,
    classifiers=[
        "Programming Language :: Python :: 3.7-3.11",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
)
