.. _temporaldata:

TemporalData
===============

.. currentmodule:: phasik.classes.TemporalData
.. autoclass:: TemporalData
    :members:
