.. _temporalnetwork:

Temporal Network
=================

.. currentmodule:: phasik.classes.TemporalNetwork
.. autoclass:: TemporalNetwork

Constructors
-------------

.. autosummary::
    :toctree: generated/
    
    TemporalNetwork
    TemporalNetwork.from_tedges
    TemporalNetwork.from_edge_timeseries
    TemporalNetwork.from_node_timeseries
    TemporalNetwork.from_static_network_and_tedges
    TemporalNetwork.from_static_network_and_edge_timeseries
    TemporalNetwork.from_static_network_and_node_timeseries
    
Utils
------

.. autosummary::
    :toctree: generated/
    
    TemporalNetwork.N
    TemporalNetwork.T
    TemporalNetwork.shape
    TemporalNetwork.number_of_edges
    TemporalNetwork.is_weighted
    TemporalNetwork.has_node
    TemporalNetwork.has_time
    TemporalNetwork.has_tedge
    
Methods for nodes and edges
------------------------------

.. autosummary::
    :toctree: generated/
    
    TemporalNetwork.add_tedges
    TemporalNetwork.neighbors
    TemporalNetwork.edge_timeseries
    TemporalNetwork.tedges_of_edge
    TemporalNetwork.tedges_of_node
    TemporalNetwork.edges_aggregated
    
Methods
--------

.. autosummary::
    :toctree: generated/
    
    TemporalNetwork.aggregated_network
    TemporalNetwork.to_partially_temporal
    TemporalNetwork.discard_temporal_info_from_edge
    TemporalNetwork.discard_temporal_info_from_node
    
.. _partiallytemporalnetwork:    
    
Partially Temporal Network
==========================

A PartiallyTemporalNetwork is a TemporalNetwork for which we do not have temporal information
about a subset of the edges. In some sense, it is in between a static and a temporal network. 

.. currentmodule:: phasik.classes.PartiallyTemporalNetwork
.. autoclass:: PartiallyTemporalNetwork

Specific methods
-----------------

It is implemented as a child class of the TemporalNetwork class, and contains the additional attributes `temporal_nodes` and `temporal_edges` and the following methods:

.. autosummary::
    :toctree: generated/
    
    PartiallyTemporalNetwork.number_of_temporal_edges
    PartiallyTemporalNetwork.number_of_temporal_nodes
    PartiallyTemporalNetwork.fraction_of_temporal_nodes
    PartiallyTemporalNetwork.fraction_of_temporal_edges
    PartiallyTemporalNetwork.temporal_neighbors
