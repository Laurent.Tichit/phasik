﻿phasik.classes.PartiallyTemporalNetwork.PartiallyTemporalNetwork.number\_of\_temporal\_nodes
============================================================================================

.. currentmodule:: phasik.classes.PartiallyTemporalNetwork

.. automethod:: PartiallyTemporalNetwork.number_of_temporal_nodes