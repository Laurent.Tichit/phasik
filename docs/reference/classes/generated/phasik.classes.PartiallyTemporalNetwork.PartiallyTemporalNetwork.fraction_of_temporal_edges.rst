﻿phasik.classes.PartiallyTemporalNetwork.PartiallyTemporalNetwork.fraction\_of\_temporal\_edges
==============================================================================================

.. currentmodule:: phasik.classes.PartiallyTemporalNetwork

.. automethod:: PartiallyTemporalNetwork.fraction_of_temporal_edges