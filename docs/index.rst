.. phasik documentation master file, created by
   sphinx-quickstart on Mon Feb 22 16:22:39 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: images/phasik_logo.svg
    :align: center


Welcome to Phasik's documentation!
==================================

.. image:: https://readthedocs.org/projects/phasik/badge/
    :target: http://phasik.readthedocs.io/

.. image:: https://codecov.io/gl/habermann_lab/phasik/branch/master/graph/badge.svg?token=8FXCNM1GSB 
 :target: https://codecov.io/gl/habermann_lab/phasik
    
.. image:: https://badge.fury.io/py/phasik.svg
    :target: https://badge.fury.io/py/phasik

.. image:: https://img.shields.io/pypi/l/phasik.svg
    :target: https://pypi.python.org/pypi/phasik/    
    
.. image:: https://static.pepy.tech/personalized-badge/phasik?period=total&units=international_system&left_color=black&right_color=green&left_text=Downloads
    :target: https://pepy.tech/project/phasik   
    

| 
| The Phasik package was created to infer temporal phases in temporal networks.  It contains various utility classes and functions that can be divided into two main parts:

1. Build, analyse, and visualise temporal networks from time series data.
2. Infer temporal phases by clustering the snapshots of the temporal network.

| Code on Gitlab: `https://gitlab.com/habermann_lab/phasik <https://gitlab.com/habermann_lab/phasik>`_   
| PiPy: `https://pypi.org/project/phasik/ <https://pypi.org/project/phasik/>`_     
    
.. toctree::
    :maxdepth: 2
    :caption: Home
    :hidden:

    about

.. toctree::
    :maxdepth: 2
    :caption: Theoretical background
    :hidden:

    theory

.. toctree::
    :maxdepth: 1
    :caption: Tutorials
    :hidden:

    tutorial/index
    tutorial/build_temporal_network
    tutorial/infer_phases

.. toctree::
    :maxdepth: 2
    :caption: API Reference
    :hidden:

    Core classes <reference/classes/index.rst>
    Drawing <reference/drawing.rst>
    Utils <reference/utils.rst>

