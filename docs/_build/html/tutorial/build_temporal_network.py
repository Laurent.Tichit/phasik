# ## Building a temporal network
#
# Phasik allows the user to build a temporal network from timeseries data in one of the following forms:
#
# * **node time series**, e.g. protein concentration over time, as a pandas Dataframe. Indices must be node names (strings or integers), and columns must be times.
#
# * **edge time series**, e.g. protein-protein interactions over time, as a pandas DataFrame. Indices must be edge names (e.g. ‘A-B’ for nodes ‘A’ and ‘B’), and columns must be times.
#
# * **t-edges**, also called *timestamped interactions* of the form (i,j,t,weight), as a pandas DataFrame. Columns must be ‘i’, ‘j’, ‘t’ and optionally ‘weight’. Each row represents one t-edge.
#
# The user can choose to build a fully connected temporal network from this data, with the following methods:
#
# Or to combine the time series data with a static network, with the following methods:
#
# ### Example from static network and node time series
#
# Before anything, start by importing Phasik and a few base packages. Numpy is only used to generate random data for the example, and Maplotlib to illustrate it.

import matplotlib.pyplot as plt
import networkx as nx  # for the static network
import numpy as np
import pandas as pd  # for the temporal data

# import Phasik
import phasik as pk

# First, we generate an example static network

edges = [('a', 'b'), ('a', 'c'), ('b', 'c'), ('a', 'd')]
static_network = nx.Graph(edges)

nx.draw_networkx(static_network)
plt.show()

# Second, we generate example time series for the nodes

nodes = list(static_network.nodes)
N = static_network.number_of_nodes()
T = 10 # number of timepoints

node_series_arr = np.random.random((N, T)) # random time series
node_series = pd.DataFrame(node_series_arr, index=nodes)
node_series

# The node time series must be in DataFrame as above. The indexes represent node names, and the columns are times. Each value in the DataFrame is that relative to a given node at a given time. For example, the 1st row could could represent the concentration of protein ‘a’ from time 0 to time 9. The time series can be visualised as follows:

node_series.T.plot()
plt.xlabel("Time")
plt.ylabel("Node time series")
plt.show()

# Finally, we generate the temporal network by combining the static network and the node time series. Several options are available to choose the normalisation of the resulting edge time series and how to deal with edges without temporal information.

# create the temporal network by combining
# the static network with the node timeseries
temporal_network = pk.TemporalNetwork.from_static_network_and_node_timeseries(
    static_network,
    node_series,
    static_edge_default_weight=1,
    normalise='minmax', # method to normalise the edge weights
    quiet=False # if True, prints less information
)

print(temporal_network)

# # To go further
#
# More examples of how to build a temporal network with Phasik can be found on the [gitlab repository](https://gitlab.com/habermann_lab/phasik).
