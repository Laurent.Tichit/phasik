Tuto test
==========


.. nbplot::

    >>> import matplotlib.pyplot as plt
    >>> import networkx as nx
    >>> import numpy as np
    >>> import pandas as pd
    >>>
    >>> import phasik as pk

.. nbplot::

    >>> # generate static network
    >>> edges = [('a', 'b'), ('a', 'c'), ('b', 'c'), ('a', 'd')]
    >>>
    >>> static_network = nx.Graph(edges)
    >>>
    >>> nodes = list(static_network.nodes)
    >>> N = static_network.number_of_nodes()
    >>>
    >>> nx.draw_networkx(static_network)
    >>> plt.show()




.. nbplot::

    >>> # generate node time series 
    >>> T = 10 # number of timepoints
    >>> node_series_arr = np.random.random((N, T))
    >>>
    >>> node_series = pd.DataFrame(node_series_arr, index=nodes) # 

.. nbplot::

    >>> node_series



.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>0</th>
          <th>1</th>
          <th>2</th>
          <th>3</th>
          <th>4</th>
          <th>5</th>
          <th>6</th>
          <th>7</th>
          <th>8</th>
          <th>9</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>a</th>
          <td>0.401917</td>
          <td>0.881092</td>
          <td>0.127991</td>
          <td>0.912639</td>
          <td>0.661475</td>
          <td>0.555443</td>
          <td>0.256126</td>
          <td>0.727078</td>
          <td>0.248045</td>
          <td>0.015757</td>
        </tr>
        <tr>
          <th>b</th>
          <td>0.852820</td>
          <td>0.936582</td>
          <td>0.988778</td>
          <td>0.107239</td>
          <td>0.126230</td>
          <td>0.250864</td>
          <td>0.463638</td>
          <td>0.491446</td>
          <td>0.369278</td>
          <td>0.982625</td>
        </tr>
        <tr>
          <th>c</th>
          <td>0.012937</td>
          <td>0.909779</td>
          <td>0.865591</td>
          <td>0.049600</td>
          <td>0.566255</td>
          <td>0.119082</td>
          <td>0.581458</td>
          <td>0.970764</td>
          <td>0.809731</td>
          <td>0.236107</td>
        </tr>
        <tr>
          <th>d</th>
          <td>0.655355</td>
          <td>0.173176</td>
          <td>0.325639</td>
          <td>0.097256</td>
          <td>0.452466</td>
          <td>0.646218</td>
          <td>0.821179</td>
          <td>0.192487</td>
          <td>0.783225</td>
          <td>0.798528</td>
        </tr>
      </tbody>
    </table>
    </div>


.. nbplot::

    >>> node_series.T.plot()
    >>> plt.xlabel("Time")
    >>> plt.ylabel("Edge time series")
    >>>
    >>> plt.show()




.. nbplot::

    >>> # create the temporal network by combining 
    >>> # the static network with the node timeseries
    >>> temporal_network = pk.TemporalNetwork.from_static_network_and_node_timeseries(
    ...     static_network, 
    ...     node_series, 
    ...     static_edge_default_weight=1,
    ...     normalise='minmax', # method to normalise the edge weights
    ... )

.. nbplot::

    >>> print(temporal_network)
    <class 'phasik.classes.TemporalNetwork.TemporalNetwork'> with 4 nodes and 10 times


