.. _tutorial:

Workflow
========

The Phasik workflow is based on the following classes: first, build a :class:`TemporalNetwork` from which to build a :class:`DistanceMatrix`, from which to then build a :class:`ClusterSet` or :class:`ClusterSets`. 

Get started:  

* `Build temporal networks <reference/tutorial/build_temporal_network.rst>`_
* `Infer phases <reference/tutorial/infer_phases.rst>`_

To go further, check out the `Jupyter Notebooks available on Gitlab <https://gitlab.com/habermann_lab/phasik>`_.They can be downloaded and run locally.