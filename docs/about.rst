.. _about:

.. image:: images/phasik_logo.svg
    :align: center
    
|

.. image:: https://readthedocs.org/projects/phasik/badge/
    :target: http://phasik.readthedocs.io/
    
.. image:: https://badge.fury.io/py/phasik.svg
    :target: https://badge.fury.io/py/phasik

.. image:: https://img.shields.io/pypi/l/phasik.svg
    :target: https://pypi.python.org/pypi/phasik/      
    
.. image:: https://static.pepy.tech/personalized-badge/phasik?period=total&units=international_system&left_color=grey&right_color=green&left_text=Downloads
    :target: https://pepy.tech/project/phasik     

|
 
What is Phasik?
================

The Phasik package was created to infer temporal phases in temporal networks.  It contains various utility classes and functions that can be divided into two main parts:

1. Build, analyse, and visualise temporal networks from time series data.
2. Infer temporal phases by clustering the snapshots of the temporal network.

Phasik was initially created for the analysis presented in the paper "`Inferring cell cycle phases from a partially temporal network of protein interactions <https://doi.org/10.1101/2021.03.26.437187>`_" by Lucas, M., Morris, A., Townsend-Teague, A., Tichit, L., Habermann, B. H., & Barrat, A. (2021), bioRxiv.

| Code on Gitlab: `https://gitlab.com/habermann_lab/phasik <https://gitlab.com/habermann_lab/phasik>`_  
| PiPy: `https://pypi.org/project/phasik/ <https://pypi.org/project/phasik/>`_ 

Installation 
================

Install the latest version of `phasik` with `pip`::

    $ pip install phasik

Alternatively, you can clone the repository manually or with `git`, go to the repository and then install locally with `pip`::
    
    $ pip install .
    
You can also simply try `phasik` by cloning the repository without installing the package.

To install for development purposes, first clone the repository and then execute

.. code:: bash

   pip install -e .['all']

If that command does not work, you may try the following instead

.. code:: zsh

   pip install -e .\[all\]

Contributing
============

If you want to contribute to this project, please make sure to read the `contributing guidelines
<https://gitlab.com/habermann_lab/phasik/-/blob/master/CONTRIBUTING.md>`_.

The best way to contribute to Phasik is by submitting a bug or request a new feature by
opening a `new issue <https://gitlab.com/habermann_lab/phasik/-/issues/new>`_.


To get more actively involved, you are invited to browse the `issues page
<https://gitlab.com/habermann_lab/phasik/-/issues>`_ and choose one that you can
work on.  The core developers will be happy to help you understand the codebase and any other doubts you may have while working on your contribution.

Contributors
=============

- Maxime Lucas (lead, ml.maximelucas[at]gmail[dot]com)
- Alex Townsend-Teague
- Arthur Morris
