Contributors
=============

- Maxime Lucas (lead, ml.maximelucas[at]gmail[dot]com)
- Alex Townsend-Teague
- Arthur Morris

Want to contribute to Phasik?
-----------------------------

Feel free to contribute by signaling a bug, suggest a new feature, or even contribute with some code. To do so, raise an issue on Gitlab:
`https://gitlab.com/habermann_lab/phasik/-/issues <https://gitlab.com/habermann_lab/phasik/-/issues>`_
