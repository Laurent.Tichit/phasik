.. _theory:
.. currentmodule:: phasik.networks.TemporalNetwork


Temporal networks
========================

Phasik works with *temporal networks*, sometimes called *dynamic networks* or *time-varying networks*. Temporal networks are networks with time-varying edges. They are a natural representation of systems that contain many interacting units (nodes), whose interactions change over time. Temporal networks are often used to model social contact networks, or brain networks, for example.

Temporal networks are a generalisation of the (static) networks in which there is no time dimension: they are composed of nodes and (constant) edges. In temporal networks, however, time is resolved and edges can vary in time. Hence, whereas static networks can be described by adjacency matrix :math:`A_{ij}`, the adjacency matrix to describe temporal networks needs to be time-varying :math:`A_{ij}(t)`. Phasik implements temporal networks as a dedicated class :class:`TemporalNetwork`. 

Phasik provides functions to build a TemporalNetwork by combining a static network to temporal data in several forms:

* time series of nodes: e.g., time series of protein concentrations 
* time series of edges: e.g., time series relative to protein-protein interactions
* a list of *t-edges*, sometimes called timestamped interactions, of the form (i,j,t,weight), where i and j are two nodes  
* a list of adjacency matrices (or snapshots), representating the time-evolution of the adjacency matrix

A particularity of Phasik is that it can build *partially* temporal networks, i.e. for which we have temporal information about only part of the edges. This is useful in many situations when using experimental data from systems in which the recording of all variables is not always possible. Phasik has a dedicated class :class:`PartiallyTemporalNetwork` for this too. 

Phasik also provides several functions to visualise temporal networks, either as static images or as animations.

For more details, see :ref:`Reference<reference>` and :ref:`Tutorial<tutorial>`.

Further reading: Holme, P., & Saramäki, J. (2012). Temporal networks. Phys. Rep., `519(3) <https://doi.org/10.1016/j.physrep.2012.03.001>`_, 97-125.

Inferring the multiscale temporal organisation of temporal networks
========================================================================

Systems often go through various phases, or states, over time. A good example of this is the cell cycle, which is typically divided into 4 main phases and multiple subphases. This multiscale temporal organisation in phases of a system can yield insight into its functioning but also its fate. That is why inferring and predicting theses phases can further our understanding of these systems. 

The function and dynamics of a network is often linked to its structure, or topology. Hence, if snapshots of the temporal network at 2 different times are very similar, they can be grouped into a same *phase*. On the contrary, if two snapshots are very different, the temporal network can be said in two different phases. This idea can be formalised by clustering snapshots of a given temporal network. Since snapshots have a 1:1 correspondence to timepoints, clusters of snapshots have a can be related to time intervals. These clusters can then be linked back to those edges most active at those times to interpret them from a microscopic standpoint. 

Further reading:

* Masuda, N., & Holme, P. (2019). `Detecting sequences of system states in temporal networks <https://doi.org/10.1038/s41598-018-37534-2>`_. Sci. Rep., 9(1) , 1-11. 
* Lucas, M., Morris, A., Townsend-Teague, A., Tichit, L., Habermann, B. H., & Barrat, A. (2021). `Inferring cell cycle phases from a partially temporal network of protein interactions <https://doi.org/10.1101/2021.03.26.437187>`_. bioRxiv.


