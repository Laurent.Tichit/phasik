# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html
# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import warnings

# -- Path setup --------------------------------------------------------------


sys.path.insert(0, os.path.abspath(".."))

warnings.filterwarnings(
    "ignore", message="Matplotlib is currently using agg", category=UserWarning
)

# -- Project information -----------------------------------------------------

VERSION_FILE = "../phasik/_version.py"
with open(VERSION_FILE, "rt") as f:
    version_txt = f.read().strip()
    VERSION = version_txt.split('"')[1]

project = "phasik"
copyright = "2021, Maxime Lucas, Alex Townsend-Teague, Arthur Morris"
author = "Maxime Lucas, Alex Townsend-Teague, Arthur Morris"
version = VERSION


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx_automodapi.automodapi",
    "sphinx_copybutton",
    "sphinx.ext.autodoc",
    "sphinx.ext.autosummary",
    "sphinx.ext.coverage",
    "sphinx.ext.napoleon",
    "sphinx_rtd_theme",
    "sphinx.ext.viewcode",
    "m2r2",
    "nb2plots",
]

# napoleon docs style options
napoleon_google_docstring = False
napoleon_use_param = False
napoleon_use_ivar = True

# copybutton options
copybutton_prompt_text = "myinputprompt"
copybutton_prompt_text = r">>> |\.\.\. |\$ |In \[\d*\]: | {2,5}\.\.\.: | {5,8}: "
copybutton_prompt_is_regexp = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# generate autosummary pages
autosummary_generate = True

# This value selects what content will be inserted into the main body of an autoclass directive.
# Both the class’ and the __init__ method’s docstring are concatenated and inserted.
autoclass_content = "both"

# If False, Sphinx renders package.module.Class as Class
add_module_names = False

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

# The name of the Pygments (syntax highlighting) style to use.
# pygments_style = 'tango'
pygments_style = "friendly"
# pygments_style = "sphinx"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
html_logo = "images/phasik_logo_small.svg"

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
html_last_updated_fmt = "%b %d, %Y"

html_theme_options = {
    "logo_only": False,
    "display_version": True,
}
