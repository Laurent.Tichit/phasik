import numpy as np

import phasik as pk


def test_convert_cluster_labels_to_dict():

    clusters_1 = np.array([3, 3, 1, 1, 2, 1])
    time_dict_1 = {3: [0, 1], 1: [2, 3, 5], 2: [4]}

    time_dict = pk.convert_cluster_labels_to_dict(clusters_1)

    assert time_dict == time_dict_1


def test_cluster_sort():

    clusters = np.array([2, 2, 2, 3, 3, 1, 1, 1])
    out = np.array([1, 1, 1, 2, 2, 3, 3, 3])
    ordered = pk.cluster_sort(clusters)
    assert np.all(ordered == out)
