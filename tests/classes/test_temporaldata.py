import numpy as np
import pandas as pd
import pytest

import phasik as pk
from phasik.classes.TemporalData import TemporalData


def test_temporal_data():
    lst = [[1, 2, 3], [4, 5, 6], [7, 8, 9], [10, 11, 12], [13, 14, 15]]
    edge_series = pd.DataFrame(lst, columns=["A", "B", "C"]).transpose()
    times = list(edge_series.columns)
    temp_data = TemporalData.from_df(
        edge_series.transpose(), times=times, true_times=times
    )

    assert temp_data.temporal_data.tolist() == lst
    assert temp_data.variables == ["A", "B", "C"]
    assert temp_data.times == [0, 1, 2, 3, 4]
    assert temp_data.true_times == [0, 1, 2, 3, 4]

    for v in temp_data.variables:
        temp_series = temp_data.series(v)
        assert list(temp_series) == list(edge_series.loc[v, :])

    temp_dict = temp_data.to_dict()
    expect_dict = {}
    for v in temp_data.variables:
        temp_series = temp_data.series(v)
        expect_dict[v] = temp_data.series(v)
    assert str(temp_dict) == str(expect_dict)

    temp_data2 = TemporalData.from_dict(
        temp_dict, temp_data.times, temp_data.true_times
    )
    assert temp_data2.temporal_data.tolist() == temp_data.temporal_data.tolist()

    df = temp_data.to_df()

    assert str(edge_series) == str(df)
