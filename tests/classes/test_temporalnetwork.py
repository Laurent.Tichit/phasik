import numpy as np
import pandas as pd

import phasik as pk


def test_hypergraph_from_static_network_and_node_timeseries(network1):

    G = network1

    N = G.number_of_nodes()
    T = 3  # number of timepoints

    nodes = list(G.nodes)
    times = list(range(T))

    node_series_arr = np.random.random((N, T))  # random time series
    node_series = pd.DataFrame(node_series_arr, index=nodes)

    TG = pk.TemporalNetwork.from_static_network_and_node_timeseries(
        G,
        node_series,
        static_edge_default_weight=1,
        normalise="minmax",  # method to normalise the edge weights
        quiet=False,  # if True, prints less information
    )

    assert TG.nodes == nodes
    assert TG.times == times

    tedges_df = TG.tedges
    assert list(tedges_df.columns) == ["i", "j", "t", "weight"]

    snaps = TG.snapshots
    assert snaps.shape == (T, N, N)

    assert len(TG) == N
    assert nodes == [n for n in TG]

    assert "a" in TG
    assert "z" not in TG

    assert TG.N() == N
    assert TG.T() == T
    assert TG.shape() == (N, T)

    assert TG.number_of_edges() == 4
    assert TG.is_weighted() is True

    assert TG.has_node("a") is True
    assert TG.has_node("z") is False

    assert TG.has_time(1) is True
    assert TG.has_time(-1) is False

    assert list(TG.neighbors().keys()) == nodes
    assert TG.edges_aggregated() == list(G.edges)

    series_ab = TG.edge_timeseries(edges=[("a", "b")])
    edge_ab = node_series_arr[0] * node_series_arr[1]
    edge_ab_ground = edge_ab - np.min(edge_ab)
    assert np.all(series_ab["a-b"] == edge_ab_ground / np.max(edge_ab_ground))

    # normalization
    for e, series in TG.edge_timeseries().items():
        assert np.max(series) == 1
        assert np.min(series) == 0

    TG_max = pk.TemporalNetwork.from_static_network_and_node_timeseries(
        G,
        node_series,
        static_edge_default_weight=1,
        normalise="max",  # method to normalise the edge weights
        quiet=False,  # if True, prints less information
    )
