import networkx as nx
import pytest


@pytest.fixture
def network1():
    edges = [("a", "b"), ("a", "c"), ("b", "c"), ("a", "d")]
    return nx.Graph(edges)
